"""
There is a file in this directory called vars.csv that has columns L1, L2, T and E.

Using the following formulas to calculate S1 and S2 and write a new file in the same format with all the original
columns as well as columns for S1 and S2.


S1 = E x (L1 x T)/12
S2 = E x (L2 x T)/12

"""
