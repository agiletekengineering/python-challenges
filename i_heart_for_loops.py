"""
This module contains some really bad code. Your task is to tidy all this code up.

Don't worry too much about the objective of the code. Main things to fix:
 - PEP8
 - Use of standard library
 - readability
"""
from random import randint


names = ['Anne', "Bob", 'Charlie', "Dan"]
Body_Parts = ['Head', "Shoulders", "Knees", "Toes"]
has_deep_voice = {"Anne": "YES", "Bob": "NO", "Charlie": "VERY", "Dan": "NO"}

months = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]

for m in months:
    for n in names:
        if n == "Dan":
            Dans = [5, 6, 7, 8, 9].pop(randint(0, 4))
            for dan in range(Dans):
                print("Dan!")
        else:
            print("In %s, %s sings:" % (m, n))
            for bp in Body_Parts:
                if bp == "Toes":
                    bp = "and " + bp
                print(bp)
            if has_deep_voice[n] == "YES":
                print("%s, you have a deep voice." % n)
            elif has_deep_voice[n] == "VERY":
                print("%s, you have a very deep voice." % n)
            if (randint(0, 1) & (has_deep_voice[n] == "YES")) or (randint(0, 1) & (has_deep_voice[n] == "VERY")):
                print(
                    "Well done {}. You are through to the next round of our very silly singing competition!".format(
                        n))
    print("\n")


